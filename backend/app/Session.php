<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = ['group_id', "schedule_id"];
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }
}
