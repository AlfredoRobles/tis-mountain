<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Group;
use App\Subject;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Group::with('user')->with('subject')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = new Group();
        $group->number = $request->number;
        $group->user_id = $request->user_id;
        $group->subject_id = $request->subject_id;
        $group->laboratory_practice = $request->laboratory_practice;
        $group = $group->save();
        return response()->json($group, 201);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Group::with('user')->with('subject')->find($id);
    }

    public function getGroupsBySubjectId($id)
    {
        return Group::with('user')->where('subject_id', $id)->get();
    }
}
