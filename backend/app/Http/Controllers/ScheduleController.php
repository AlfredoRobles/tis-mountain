<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Schedule;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Schedule::with('day')->with('hour')->with('laboratory')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = new Schedule();
        $schedule->day_id = $request->day_id;
        $schedule->hour_id = $request->hour_id;
        $schedule->laboratory_id = $request->laboratory_id;
        $schedule = $schedule->save();
        return response()->json($schedule, 201);
    }
      
}
