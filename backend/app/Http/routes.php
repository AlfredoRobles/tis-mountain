<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('users', 'Auth\AuthController@register');
Route::get('users', 'Auth\AuthController@index');
Route::get('users/{user}', 'Auth\AuthController@show');
Route::get('articles', 'ArticleController@index');
Route::get('articles/{article}', 'ArticleController@show');
Route::post('articles', 'ArticleController@store');
Route::put('articles/{article}', 'ArticleController@update');
Route::delete('articles/{article}', 'ArticleController@delete');
Route::get('laboratories', 'LaboratoryController@index');
Route::post('laboratories', 'LaboratoryController@store');
Route::get('laboratories/{laboratory}', 'LaboratoryController@show');
Route::get('subject', 'SubjectController@index');
Route::get('subject/{subject}', 'SubjectController@show');
Route::get('groups/subject/{subject}', 'GroupController@getGroupsBySubjectId');
Route::post('subject', 'SubjectController@store');
Route::post('sessions', 'SessionController@store');
Route::get('sessions', 'SessionController@index');
Route::delete('sessions/{group}', 'SessionController@delete');
Route::get('sessions/group/{group}', 'SessionController@getByGroup');
Route::get('group', 'GroupController@index');
Route::get('groups/{group}', 'GroupController@show');
Route::post('groups/{group}/event/add-schedule', 'GroupController@addSchedule');
Route::patch('groups/{group}/event/remove-schedule', 'GroupController@removeSchedule');
Route::get('schedules', 'ScheduleController@index');
Route::post('schedules', 'ScheduleController@store');
Route::get('groups', 'GroupController@index');
Route::post('groups', 'GroupController@store');
Route::get('days', 'DayController@index');
Route::get('hours', 'HourController@index');
Route::post('users/{user}/event/register-to-session', 'SessionStudentController@store');
