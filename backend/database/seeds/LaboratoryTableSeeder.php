<?php

use Illuminate\Database\Seeder;
use App\Laboratory;

class LaboratoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Laboratory::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Laboratory::create([
                'name' => $faker->sentence,
                'capacity' => $faker->randomDigit,
                'machineType' => $faker->sentence
            ]);
        }
    }
}
