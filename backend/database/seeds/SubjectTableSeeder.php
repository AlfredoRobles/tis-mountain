<?php

use Illuminate\Database\Seeder;
use App\Laboratory;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        subject::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Subject::create([
                'name' => $faker->sentence,
                'code' => $faker->sentence
            ]);
        }
    }
}