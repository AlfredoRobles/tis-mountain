
INSERT INTO `day` (`id`, `value`, `order`)
VALUES (1,'Lunes',1), (2,'Martes',2), (3,'Miercoles',3), (4,'Jueves',4), (5,'Viernes',5);

INSERT INTO `hour` (`id`, `value`, `order`)
VALUES (1,'6:45-8:15',1), (2,'8:15-9:45',2), 
(3,'9:45-11:15',3), (4,'11:15-12:45',4), (5,'12:45-14:15',5),
(6,'14:15-15:45',6), (7,'15:45-17:15',7), (8,'17:15-18:45',8), 
(9,'18:45-20:15',9), (10,'20:15-21:45',10);

INSERT INTO user
VALUES (100001, "juan.perez@univ.edu", "Perez Perez", "Juan Alberto", "Password123!", "Admin", "201000125"),
(100002, "maria.perez@univ.edu", "Perez Soleto", "Maria Andrea", "Password123!", "Docente", "201000126"),
(100003, "marcos.montan@univ.edu", "Montan Quispe", "Marcos Alberto", "Password123!", "Estudiante", "205000125"),
(100004, "lucia.zurita@univ.edu", "Surita Mance", "Lucia Marta", "Password123!", "Auxiliar", "201005125"),
/* Estudiantes */
(20000, "dany.monzon@univ.edu", "Araya Monzon", "Dany", "Password123!", "Estudiante", "201100900"),
(20001, "jose.arevalo@univ.edu", "Arevalo", "Jose", "Password123!", "Estudiante", "201100901"),
(20002, "andy.camacho@univ.edu", "Camacho Centella", "Andy", "Password123!", "Estudiante", "201100902"),
(20003, "alejandra.montano@univ.edu", "Montaño", "Alejandra", "Password123!", "Estudiante", "201100903"),
(20004, "luana.orellana@univ.edu", "Orellana Padilla", "Luana Iris", "Password123!", "Estudiante", "201100904"),
(20005, "adrian.perez@univ.edu", "Perez Zubieta", "Adrian", "Password123!", "Estudiante", "201100905"),
(20006, "ernesto.zurita@univ.edu", "Zurita Rosas", "Ernesto", "Password123!", "Estudiante", "201100906"),
(20007, "daniela.baldiviezo@univ.edu", "Baldiviezo Rosas", "Daniela", "Password123!", "Estudiante", "201100907"),
(20008, "dayana.poveda@univ.edu", "Poveda Claure", "Dayana", "Password123!", "Estudiante", "201100908"),
(20009, "miguel.rojas@univ.edu", "Rojas", "Miguel Angel", "Password123!", "Estudiante", "201100909"),
(20010, "daniel.soliz@univ.edu", "Soliz Garcia", "Daniel", "Password123!", "Estudiante", "201100910"),
(20011, "andrea.guillen@univ.edu", "Guillen Colque", "Andrea Giovana", "Password123!", "Estudiante", "201100911"),
(20012, "gabriel.lopez@univ.edu", "Lopez Arze", "Gabriel Hector", "Password123!", "Estudiante", "201100912"),
(20013, "jorge.delgadillo@univ.edu", "Delgadillo Quinteros", "Jorge Miguel", "Password123!", "Estudiante", "201100913"),
(20014, "oscar.ptismountainacheco@univ.edu", "Pachecho Arias", "Oscar Manuel", "Password123!", "Estudiante", "201100914"),
(20015, "ronal.sanabria@univ.edu", "Sanabria Siles", "Ronaldo", "Password123!", "Estudiante", "201100915"),
(20016, "eduardo.quispe@univ.edu", "Quispe Herbas", "Eduardo Rodrigo", "Password123!", "Estudiante", "201100916"),
(20017, "rodrigo.soria@univ.edu", "Soria Martinez", "Rodrigo", "Password123!", "Estudiante", "201100917"),
(20018, "ted.mosby@univ.edu", "Mosby", "Teodore Evelyn", "Password123!", "Estudiante", "201100918"),
(20019, "barney.stinson@univ.edu", "Stinson", "Barney", "Password123!", "Estudiante", "201100919"),
/* Auxiliares */
(20020, "fernando.rojas@univ.edu", "Rojas Barja", "Fernando Carlos", "Password123!", "Estudiante", "201100920"),
(20021, "kadik.becerra@univ.edu", "Becerra Soliz", "Kadik", "Password123!", "Estudiante", "201100921"),
(20022, "ronal.bautista@univ.edu", "Bautista Quispe", "Ronal", "Password123!", "Estudiante", "201100922"),
(20023, "sergio.iriarte@univ.edu", "Iriarte", "Sergio Marcelo", "Password123!", "Estudiante", "201100923"),
(20024, "bruno.diaz@univ.edu", "Diaz", "Bruno", "Password123!", "Estudiante", "201100928"),
/* Docentes */
(20025, "jamil.arancibia@univ.edu", "Arancibia Siles", "Jamil", "Password123!", "Estudiante", "201100924"),
(20026, "leticia.blanco@univ.edu", "Blanco Coca", "Leticia", "Password123!", "Estudiante", "201100925"),
(20027, "vldaimir.costas@univ.edu", "Costas", "Vladimir", "Password123!", "Estudiante", "201100926"),
(20028, "frank.villarroel@univ.edu", "Villarroel", "Henry Frank", "Password123!", "Estudiante", "201100927");
/* Materias */
INSERT INTO `subject`
VALUES(100,"001","Introduccion A La Programacion"),
(101,"003","Elementos De Programacion"),
(102,"002","Taller De Programacion");
/* Laboratorios */
INSERT INTO `laboratory`
VALUES(200,30,"Laboratorio 1","i7, 8gb RAM"),
(201,25,"Laboratorio 2","virtualizadas"),
(202,20,"Laboratorio 3","core duo, 4gb RAM");
/* grupos */

