
# TIS Mountain

Servicios Backend de TIS Mountain, este proyecto fue creado usando Laravel 5.1

## Documentacion oficial

La documentacion oficial del framework se encuentra en el [sitio web de Laravel](http://laravel.com/docs/5.1).

## Practic Guideline

Una guia practica de como realizar REST API con laravel puede ser encontrada [aqui](https://www.toptal.com/laravel/restful-laravel-api-tutorial)

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
