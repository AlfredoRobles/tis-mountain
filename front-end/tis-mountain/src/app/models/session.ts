export class Session {
    id?: number;
    day: string;
    hour: string;
    labId: number;
    groupId: number;
}
