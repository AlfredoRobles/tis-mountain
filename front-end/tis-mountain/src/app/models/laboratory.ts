export class Laboratory {
    id?: number;
    name: string;
    capacity: number;
    machineType: string;
}
