export class Group {
    id?: number;
    number: string;
    subject: string;
    teacher: string;
}