import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  roles;
  currentUser;

  constructor(private authService: AuthService,
    private router: Router) {
    this.roles = authService.getRoles();
    setTimeout(a => {
      this.currentUser = authService.getCurrentUser();
    }, 3000);
  }

  changeRole(userRole) {
    this.authService.setCurrentUser(userRole);
    this.currentUser = this.authService.getCurrentUser();
    this.router.navigate(['']);
  }

}
