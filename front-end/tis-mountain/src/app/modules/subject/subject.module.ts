import { CommonModule } from '@angular/common';
import { CreateSubjectComponent } from './create-subject/create-subject.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { AdminGuardService } from '../../services/routes-guard.service';

import { SubjectService } from '../../services/subject.service';
const routes: Routes = [
  { path: '', component: CreateSubjectComponent, canActivate: [AdminGuardService] }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    SubjectService,
    ToastrService
  ],
  declarations: [CreateSubjectComponent]
})
export class SubjectModule { }
