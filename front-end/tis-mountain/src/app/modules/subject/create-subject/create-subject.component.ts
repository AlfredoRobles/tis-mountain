import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { SubjectService } from '../../../services/subject.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-subject',
  templateUrl: './create-subject.component.html'
})
export class CreateSubjectComponent implements OnInit {

  subjectForm: FormGroup;

  constructor(private service: SubjectService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router) {
      this.subjectForm = this.createSubjectFrom();
    }

  ngOnInit() { }

  registerSubject(): void {
    this.service.registerSubject(this.subjectForm.value)
    .then(data => {
      this.showMessage('Materia registrado correctamente', 'alert alert-info alert-with-icon');
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.showMessage('Materia no registrado', 'alert alert-warning alert-with-icon')
    });
  }

  cancel(): void {
    this.router.navigate(['/']);
  }

  private createSubjectFrom(){
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      code: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]]
    });
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });

  }

}
