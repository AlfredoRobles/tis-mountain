import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuxiliarSessionsComponent } from './auxiliar-sessions.component';

describe('AuxiliarSessionsComponent', () => {
  let component: AuxiliarSessionsComponent;
  let fixture: ComponentFixture<AuxiliarSessionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuxiliarSessionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuxiliarSessionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
