import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auxiliar-sessions',
  templateUrl: './grade-session.component.html'
})
export class GradeSessionComponent implements OnInit {

  sessionStudents= [
    {
        student: 'Araya Dany',
        assistance: true,
        activity: ''
    },
    {
        student: 'Arevalo Jose',
        assistance: true,
        activity: ''
    },
    {
        student: 'Camacho Andy',
        assistance: false,
        activity: ''
    },
    {
        student: 'Montaño Alejandra',
        assistance: true,
        activity: ''
    },
    {
        student: 'Orellana Luana',
        assistance: true,
        activity: ''
    },
    {
        student: 'Perez Adrian',
        assistance: false,
        activity: ''
    },
    {
        student: 'Zurita Ernesto',
        assistance: true,
        activity: ''
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
