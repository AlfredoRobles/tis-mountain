import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../../services/group.service';
import { UsersService } from '../../../services/users.service';
import { DayTimeService } from '../../../services/day-time.service';
import { SubjectService } from '../../../services/subject.service';
import { SessionService } from '../../../services/session.service';
import { RegisterToScheduleService } from '../../../services/register-to-schedule.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-to-session',
  templateUrl: './register-to-session.component.html'
})
export class RegisterToSessionComponent implements OnInit {
  subjects = [];
  groups = [];
  sessions = [];
  registerForm: FormGroup;
  constructor(
    private dayTimeService: DayTimeService,
    private groupService: GroupService,
    private formBuilder: FormBuilder,
    private userService: UsersService,
    private sessionService: SessionService,
    private subjectService: SubjectService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private registerToScheduleService: RegisterToScheduleService) {
    this.registerForm = this.createFormGroup();
  }

  ngOnInit() {
    this.subjectService.getAllSubjects()
      .then(response => {
        this.subjects = response;
      });
  }

  selectSubject() {
    this.registerForm.get('group').reset()
    this.registerForm.get('sessionId').reset()
    this.groups = [];
    this.sessions = [];
    this.groupService.getGroupsBySubject(this.registerForm.get('subject').value)
      .then(response => {
        this.groups = response;
      });
  }

  selectGroup() {
    this.sessions = [];
    this.registerForm.get('sessionId').reset()
    this.sessionService.getSessionsByGroup(this.registerForm.get('group').value.id)
      .then(response => {
        this.sessions = response;
      });
  }

  register() {
    this.userService.registerToSession(this.route.snapshot.paramMap.get('id'), this.registerForm.value)
    .then(response => {
      this.showMessage('Su registro fue completado', 'alert alert-info');
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.showMessage('No se pudo registrar', 'alert alert-warning');
    });
  }
  
  private createFormGroup() {
    return this.formBuilder.group({
      subject: ['', Validators.required],
      group: ['', Validators.required],
      sessionId: ['', Validators.required]
    });
  }

  cancel(): void {
    this.router.navigate(['/']);
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });
  }
}
