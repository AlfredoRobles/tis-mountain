import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { SessionService } from '../../services/session.service';
import { GroupService } from '../../services/group.service';
import { LaboratoryService } from '../../services/laboratory.service';
import { CreateSessionComponent } from './create-session/create-session.component'
import { RegisterToSessionComponent } from './register-to-session/register-to-session.component';
import { DayTimeService } from '../../services/day-time.service';
import { SubjectService } from '../../services/subject.service';
import { RegisterToScheduleService } from '../../services/register-to-schedule.service';
import { UsersService } from '../../services/users.service';
import { AdminGuardService, StudentGuardService, AssistantGuardService } from '../../services/routes-guard.service';
import { AuxiliarSessionsComponent } from './auxiliar-sessions/auxiliar-sessions.component';
import { GradeSessionComponent } from './grade-session/grade-session.component';

const routes: Routes = [
  { path: '', component: CreateSessionComponent, canActivate: [AdminGuardService] },
  { path: 'register-student/:id', component: RegisterToSessionComponent, canActivate: [StudentGuardService] },
  { path: 'auxiliar-sessions', component: AuxiliarSessionsComponent, canActivate: [AssistantGuardService] },
  { path: 'grade-session', component: GradeSessionComponent, canActivate: [AssistantGuardService] },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    SessionService,
    GroupService,
    UsersService,
    LaboratoryService,
    DayTimeService,
    SubjectService,
    RegisterToScheduleService,
    ToastrService
  ],
  declarations: [CreateSessionComponent, RegisterToSessionComponent, AuxiliarSessionsComponent, GradeSessionComponent]
})
export class SessionModule { }
