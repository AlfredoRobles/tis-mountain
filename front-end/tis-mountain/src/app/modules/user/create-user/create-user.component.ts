import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User } from '../../../models/user';
import { UsersService } from '../../../services/users.service';
import { FormBuilder } from '@angular/forms';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html'
})
export class CreateUserComponent implements OnInit {

  user = new User;
  roles = [{ 'name': 'Docente' }, { 'name': 'Estudiante' }, { 'name': 'Auxiliar' }];
  constructor(private service: UsersService, private toastr: ToastrService,
    private router: Router, private fb: FormBuilder) {
    this.user.role = this.roles[0].name;
  }

  ngOnInit() {
  }

  saveUser(): void {
    if (isNullOrUndefined(this.user.email) || isNullOrUndefined(this.user.name)
      || isNullOrUndefined(this.user.lastName) || isNullOrUndefined(this.user.sisCode)) {
      this.showMessage('Campo obligatorio no llenado, recuerde llenar los marcados con (*)', 'alert alert-warning');
    } else {
      this.user.password = 'secret123';
      console.log(this.user);
      this.service.registerUser(this.user).subscribe(
        res => {
          this.showMessage('Usuario registrado correctamente', 'alert alert-info');
          this.router.navigate(['/']);
        },
        err => {
          this.showMessage('Usuario no registrado', 'alert alert-warning');
        });
    }

  }

  cancel(): void {
    this.router.navigate(['/']);
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });

  }
}
