import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateUserComponent } from './create-user/create-user.component';
import { UsersService } from '../../services/users.service';
import { AdminGuardService } from '../../services/routes-guard.service';

const routes: Routes = [
  { path: '', component: CreateUserComponent, canActivate: [AdminGuardService] }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    UsersService,
    ToastrService
  ],
  declarations: [CreateUserComponent],
  exports: [
    ReactiveFormsModule
  ]
})
export class UserModule { }
