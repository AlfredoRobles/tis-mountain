import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GroupService } from '../../../services/group.service';
import { ScheduleService } from '../../../services/schedule.service';
import { SessionService } from '../../../services/session.service';

@Component({
  selector: 'app-add-schedule',
  templateUrl: './add-schedule.component.html',
  styleUrls: ['./add-schedule.component.css']
})
export class AddScheduleComponent implements OnInit {
  scheduleForm: FormGroup;
  group;
  schedules = [];
  sessions = [];
  constructor(private toastr: ToastrService, private groupService: GroupService,
    private scheduleService: ScheduleService,
    private sessionService: SessionService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) {
    this.scheduleForm = this.createFormSchedule();
  }

  ngOnInit() {
    let promiseGetGroup = this.groupService.getGroupById(this.route.snapshot.paramMap.get('id'))
    let promiseGetSession = this.sessionService.getSessionsByGroup(this.route.snapshot.paramMap.get('id'))
    let promiseGetAllSchedules = this.scheduleService.getAllSchedule()
    Observable.forkJoin([promiseGetGroup, promiseGetSession, promiseGetAllSchedules])
      .subscribe((response) => {
        this.group = response[0]
        this.sessions = response[1]

        this.schedules = response[2].filter(
          schedule => !this.sessions.some(session => session.schedule.id === schedule.id));
        console.log(this.schedules)
      });
  }

  addSchedules() {
    let schedules = this.scheduleForm.get('schedules').value
    for (let schedule of schedules) {
      this.addSchedule(schedule);
    }
  }

  private addSchedule(schedule) {
    let session = {'groupId': this.group.id, 'scheduleId': schedule.id}
    this.sessionService.registerSession(session)
      .then(data => {
        this.showMessage('Horario agregado correctamente', 'alert alert-info');
        data.schedule = schedule
        this.sessions.push(data)
        const index = this.schedules.indexOf(schedule, 0);
        if (index > -1) {
          this.schedules.splice(index, 1);
        }
      })
      .catch(error => {
        console.log(error)
        this.showMessage('No se pudo agregar el horario', 'alert alert-warning');
      });
  }

  removeSchedule(session) {
    this.sessionService.delteSession(session.id)
      .then(data => {
        this.showMessage('Horario removido correctamente', 'alert alert-info');
        this.schedules.push(session.schedule)
        const index = this.sessions.indexOf(session, 0);
        if (index > -1) {
          this.sessions.splice(index, 1);
        }
      })
      .catch(error => {
        console.log(error)
        this.showMessage('No se pudo remover el horario', 'alert alert-warning');
      });
  }

  private createFormSchedule() {
    return this.formBuilder.group({
      schedules: ['', [Validators.required]]
    });
  }

  cancel(): void {
    this.router.navigate(['/']);
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });
  }
}
