import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AdminGuardService } from '../../services/routes-guard.service';

import { CreateLaboratoryComponent } from './create-laboratory/create-laboratory.component';
import { LaboratoryService } from '../../services/laboratory.service';

const routes: Routes = [
  { path: '', component: CreateLaboratoryComponent, canActivate: [AdminGuardService] }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    LaboratoryService,
    ToastrService
  ],
  declarations: [CreateLaboratoryComponent]
})
export class LaboratoryModule { }
