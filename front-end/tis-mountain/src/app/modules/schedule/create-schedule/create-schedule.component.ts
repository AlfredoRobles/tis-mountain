import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { ScheduleService } from '../../../services/schedule.service';
import { GroupService } from '../../../services/group.service';
import { LaboratoryService } from '../../../services/laboratory.service';
import { DayTimeService } from '../../../services/day-time.service';
import { Observable } from 'rxjs/Rx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-schedule',
  templateUrl: './create-schedule.component.html'
})
export class CreateScheduleComponent implements OnInit {

  scheduleForm: FormGroup;
  days: any;
  hours: any;
  laboratories: any;
  
  constructor(private toastr: ToastrService, private router: Router,
    private scheduleService: ScheduleService, private laboratyService: LaboratoryService,
    private formBuilder: FormBuilder,
    private dayTimeServise: DayTimeService) { 
      this.scheduleForm = this.createMyForm();
  }

  ngOnInit() { 
    let promiseGetDays = this.dayTimeServise.getDays()
    let promiseGetHours = this.dayTimeServise.getHours()
    let promiseAllLaboratory = this.laboratyService.getAllLaboratory()
    Observable.forkJoin([promiseGetDays, promiseGetHours, promiseAllLaboratory])
       .subscribe((response) => {
        this.days = response[0]
        this.hours = response[1]  
        this.laboratories = response[2]
       });
  }

  private createMyForm(){
    return this.formBuilder.group({
      day_id: ['', Validators.required],
      hour_id: ['', Validators.required],
      laboratory_id: ['', Validators.required]
    });
  }

  registerSchedule(){
    this.scheduleService.registerSchedule(this.scheduleForm.value)
    .then(data => {
      this.showMessage('Horario registrado correctamente', 'alert alert-info alert-with-icon');
      this.router.navigate(['/']);
    })
    .catch(error => {
      this.showMessage('Horario no registrado', 'alert alert-warning alert-with-icon');
    });
  }

  showMessage(message: string, toastClass: string) {
    this.toastr.info('<span class="now-ui-icons ui-1_bell-53"></span>' + message + '</b>.', '', {
      timeOut: 8000,
      closeButton: false,
      enableHtml: true,
      toastClass: toastClass,
      positionClass: 'toast-' + 'top' + '-' + 'right'
    });
  }

  cancel(): void {
    this.router.navigate(['/']);
  }
}
