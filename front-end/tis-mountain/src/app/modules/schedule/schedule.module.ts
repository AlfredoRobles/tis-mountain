import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { SessionService } from '../../services/session.service';
import { GroupService } from '../../services/group.service';
import { LaboratoryService } from '../../services/laboratory.service';
import { DayTimeService } from '../../services/day-time.service';
import { SubjectService } from '../../services/subject.service';
import { RegisterToScheduleService } from '../../services/register-to-schedule.service';
import { ScheduleService } from '../../services/schedule.service';
import { CreateScheduleComponent } from './create-schedule/create-schedule.component';
import { AdminGuardService } from '../../services/routes-guard.service';

const routes: Routes = [
  { path: '', component: CreateScheduleComponent, canActivate: [AdminGuardService] }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    SessionService,
    GroupService,
    LaboratoryService,
    DayTimeService,
    SubjectService,
    RegisterToScheduleService,
    ToastrService,
    ScheduleService
  ],
  declarations: [CreateScheduleComponent]
})
export class ScheduleModule { }
