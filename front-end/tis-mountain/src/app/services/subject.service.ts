import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Subject } from '../models/subject';

@Injectable()
export class SubjectService {
    route = 'http://localhost:8080/subjects';

    constructor(private http: Http) { }

    registerSubject(subject: Subject) {
        return this.http.post(this.route, subject).toPromise();
    }

    getAllSubjects() {
        return this.http.get(this.route)
        .map(res => {
            return res.json()
        }).toPromise();
    }
}
