import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { User } from '../models/user';
import { mapChildrenIntoArray } from '@angular/router/src/url_tree';

@Injectable()
export class UsersService {
    route = 'http://localhost:8080/users';

    user: User = {
        id:7,
        sisCode:201400908,
        name:"pepe",
        lastName:"roman",
        role:"Estudiante",
        email:"exa@mail.com"
    };

    constructor(private http: Http) { }

    registerUser(user: User) {
        return this.http.post(this.route, user);
    }

    getAllUsers() {
        return this.http.get(this.route)
            .map(res => {
                return res.json()
            }).toPromise();
    }

    getUserById(id: number) {
        return this.http.get(this.route + '/' + id);
    }

    getUserByRole(role) {
        return this.http.get(this.route + '/' + role);
    }

    getUserActive() {
        return this.user;
    }

    registerToSession(userId, session) {
        return this.http.post(this.route + '/'+userId+'/event/register-to-session', session)
            .map(res => {
                return res.json()
            }).toPromise();
    }
}
