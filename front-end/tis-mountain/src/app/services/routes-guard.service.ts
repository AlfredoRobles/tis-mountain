import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AdminGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.userIsAdmin()) {
      return true;
    } else {
      this.router.navigate([''], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }

}

@Injectable()
export class StudentGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.userIsStudent()) {
      return true;
    } else {
      this.router.navigate([''], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }

}

@Injectable()
export class AssistantGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.userIsAssistant()) {
      return true;
    } else {
      this.router.navigate([''], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }

}
