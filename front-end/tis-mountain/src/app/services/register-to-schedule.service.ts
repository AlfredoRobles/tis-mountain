import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';

@Injectable()
export class RegisterToScheduleService {

  route = 'http://localhost:8080/session-items';
  constructor(private http: Http) { }
  sessionItem = {
    session: '',
    user: ''
  };
  registerToSession(registrationData, user) {
    console.log(registrationData);
    this.sessionItem.session = registrationData.scheduleItem.session;
    this.sessionItem.user = user;
    return this.http.post(this.route, this.sessionItem);
  }

}
