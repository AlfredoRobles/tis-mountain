import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Laboratory } from '../models/laboratory';
import 'rxjs/add/operator/map';

@Injectable()
export class LaboratoryService {
    route = 'http://localhost:8080/laboratories';

    constructor(private http: Http) { }

    registerLaboratory(laboratory: Laboratory) {
        return this.http.post(this.route, laboratory);
    }

    getAllLaboratory(){
        return this.http.get(this.route)
        .map(res => {
            return res.json()
        })
        .toPromise();
    }

    getLaboratoryById(id: number) {
        return this.http.get(this.route + '/' + id);
    }
}
