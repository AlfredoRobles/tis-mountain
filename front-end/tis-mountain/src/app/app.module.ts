import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { AuthService } from './services/auth.service';
import { UsersService } from './services/users.service';
import {AdminGuardService, StudentGuardService, AssistantGuardService} from './services/routes-guard.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ToastrModule.forRoot()
  ],
  providers: [
    AuthService,
    AdminGuardService,
    StudentGuardService,
    AssistantGuardService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
