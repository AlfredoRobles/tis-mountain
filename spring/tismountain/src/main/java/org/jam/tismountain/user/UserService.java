/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.user;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mauricio Rojas
 */
@Service
public class UserService {
    
    @Autowired
    private UserRepository userRepository;
    
    public User save(@Valid User user) {
        return userRepository.save(user);
    }
    
    public User getById(Long id){
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User Not Found"));
    }
}
