/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.hour;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mauricio Rojas
 */
@Service
public class HourService {
    
    @Autowired
    private HourRepository hourRepository;

    public Hour save(@Valid Hour hour) {
        return hourRepository.save(hour);
    }
}
