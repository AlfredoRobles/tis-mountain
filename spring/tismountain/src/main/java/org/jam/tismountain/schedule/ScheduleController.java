/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.schedule;

import javax.validation.Valid;
import org.jam.tismountain.laboratory.Laboratory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Mauricio Rojas
 */
@RepositoryRestController
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private LocalValidatorFactoryBean validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.addValidators(validator);
    }
    
    @PostMapping(path = "schedules")
    public ResponseEntity<Schedule> save(@RequestBody @Valid Schedule schedule) {
        return new ResponseEntity<>(scheduleService.save(schedule), HttpStatus.CREATED);
    }
}
