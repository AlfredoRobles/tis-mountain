/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.schedule;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import org.jam.tismountain.day.Day;
import org.jam.tismountain.hour.Hour;
import org.jam.tismountain.laboratory.Laboratory;

/**
 *
 * @author Mauricio Rojas
 */
@Entity
@Table(name = "SCHEDULE")
@Data
public class Schedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne
    private Day day;

    @ManyToOne
    private Hour hour;

    @ManyToOne
    private Laboratory laboratory;
}
