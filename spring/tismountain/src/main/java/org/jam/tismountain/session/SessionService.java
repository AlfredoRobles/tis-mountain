/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.session;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.jam.tismountain.group.Group;
import org.jam.tismountain.group.GroupService;
import org.jam.tismountain.session.sessionStudent.SessionStudent;
import org.jam.tismountain.session.sessionStudent.SessionStudentRepository;
import org.jam.tismountain.user.User;
import org.jam.tismountain.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author grymlock
 */
@Service
public class SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private SessionStudentRepository studentSessionRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    public Session save(@Valid Session session) {
        return sessionRepository.save(session);
    }

    public Session getById(long id) {
        return sessionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Session Not Found"));
    }

    public List<Session> getSessionsByGroup(Long groupId) {
        Group group = groupService.getById(groupId);
        List<Session> sessions = sessionRepository.findAll();
        return sessions.stream().filter(session -> session.getGroup().equals(group)).collect(Collectors.toList());
    }

    public SessionStudent registerStudentToSession(Long userId, SessionStudent sessionStudent) {
        User user = userService.getById(userId);
        SessionStudent currentSessionStudent = new SessionStudent();
        currentSessionStudent.setUser(user);
        currentSessionStudent.setSession(sessionStudent.getSession());
        return studentSessionRepository.save(currentSessionStudent);

    }

    public Session deleteSession(Long sessionId) {
        Session sessionToDelete = getById(sessionId);
        sessionRepository.delete(sessionToDelete);
        return sessionToDelete;

    }
}
