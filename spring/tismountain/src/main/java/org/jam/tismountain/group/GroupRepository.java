/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.group;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Mauricio Rojas
 */
@RepositoryRestResource(path = "groups")
public interface GroupRepository extends JpaRepository<Group, Long>{
    
}
