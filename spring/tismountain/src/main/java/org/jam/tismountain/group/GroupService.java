/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.group;

import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.jam.tismountain.schedule.Schedule;
import org.jam.tismountain.schedule.ScheduleService;
import org.jam.tismountain.subject.Subject;
import org.jam.tismountain.subject.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mauricio Rojas
 */
@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;
    
    @Autowired
    private SubjectService subjectService;
    
    @Autowired
    private ScheduleService scheduleService;

    public Group save(@Valid Group group) {
        return groupRepository.save(group);
    }

    public Group getById(Long id) {
        return groupRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Group Not Found"));
    }
    
    public List<Group> getGroupsBySubject(Long SubjectId){
        Subject subject = subjectService.getById(SubjectId);
        List<Group> groups = groupRepository.findAll();
        return groups.stream().filter(group -> group.getSubject().equals(subject)).collect(Collectors.toList());
    }

    public Group addSchedule(Long groupId, Schedule schedule) {
        return null;
    }
    
    public Group removeSchedule(Long groupId, Schedule schedule) {
        return null;
    }

}
