/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.subject;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mauricio Rojas
 */
@Service
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;
    
    public Subject save(@Valid Subject subject) {
        return subjectRepository.save(subject);
    }
    
    
    public Subject getById(Long id){
        return subjectRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Subject Not Found"));
    }
    
}
