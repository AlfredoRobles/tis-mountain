/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.schedule;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mauricio Rojas
 */
@Service
public class ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;
    
    public Schedule save(@Valid Schedule schedule) {
        return scheduleRepository.save(schedule);
    }
}
