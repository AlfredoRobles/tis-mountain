/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.hour;

import java.util.Optional;
import javax.validation.constraints.NotBlank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author Mauricio Rojas
 */
@RepositoryRestResource(path = "hours")
public interface HourRepository extends JpaRepository<Hour, Long> {

    Optional<Hour> findByOrder(@NotBlank int order);
}
