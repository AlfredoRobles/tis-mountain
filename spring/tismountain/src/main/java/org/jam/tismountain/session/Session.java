/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.session;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import org.jam.tismountain.group.Group;
import org.jam.tismountain.schedule.Schedule;

/**
 *
 * @author Mauricio Rojas
 */
@Entity
@Table(name = "SESSION")
@Data
public class Session implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne
    private Group group;

    @ManyToOne
    private Schedule schedule;
}
