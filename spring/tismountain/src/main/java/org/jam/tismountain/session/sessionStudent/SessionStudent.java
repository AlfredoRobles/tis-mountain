/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.session.sessionStudent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import org.jam.tismountain.session.Session;
import org.jam.tismountain.user.User;

/**
 *
 * @author grymlock
 */
@Entity
@Table(name = "SESSION_STUDENT")
@Data
public class SessionStudent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    
    @ManyToOne
    private Session session;
    
    @ManyToOne
    private User user;
}
