/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jam.tismountain.day;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Mauricio Rojas
 */
@RepositoryRestController
public class DayController {
    
    @Autowired
    private DayService dayService;
    
    @Autowired
    private LocalValidatorFactoryBean validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.addValidators(validator);
    }
    
    @PostMapping(path = "days")
    public ResponseEntity<Day> save(@RequestBody @Valid Day day) {
        return new ResponseEntity<>(dayService.save(day), HttpStatus.CREATED);
    }
}
